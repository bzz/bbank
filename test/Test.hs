module Test where

import qualified Data.ByteString.Char8 as BS
import           Data.Maybe
import           Keys
import           Test.QuickCheck


genSafeChar :: Gen Char
genSafeChar = elements (['a'..'f'] ++ ['0'..'9'])

genSafeString :: Gen String
genSafeString = vectorOf 64 genSafeChar



newtype SafeString = SafeString { unwrapSafeString :: String }
  deriving Show

instance Arbitrary SafeString where
  arbitrary = SafeString <$> genSafeString


-- addressFromSecretKey :: String -> String
-- addressFromSecretKey x
--   | x == "18e14a7b6a307f426a94f8114701e7c8e774e7f9a47e2c2035db29a206321725" = "1PMycacnJaSqwwJqjawXBErnLsZ7RkXUAs"
--   | otherwise = ""



testhex (SafeString s) =
  ( showHex . packHex $ s ) == s
testwif (SafeString s) =
   ( showHex . secretFromWIF . wifFromSecret $ packHex s ) == s

test1wifmainnet = case BS.unpack . wifFromSecret $ packHex "0C28FCA386C7A227600B2FE50B7CAE11EC86D3BF1FBE471BE89827E19D72AA1D" of
    "KwdMAjGmerYanjeui5SHS7JkmpZvVipYvB2LJGU1ZxJwYvP98617" -> return ()
    s                                                      -> fail s



main = do
  quickCheck testhex
  quickCheck testwif
  test1wifmainnet





-- main :: IO ()
-- main = test_wif



