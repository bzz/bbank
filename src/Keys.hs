--{-# LANGUAGE OverloadedStrings #-}

module Keys
  (
    segwitAddressFromWIF
  , segwitAddressFromSecret
  , compressedAddressFromSecret
  , uncompressedAddressFromSecret
  , wifFromSecret
  , secretFromWIF
  , wifValid
  )
  where

import qualified Data.Binary                as BIN
import           Data.Bool
import qualified Data.ByteString.Char8      as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import           Data.List
import           Data.Maybe
--import qualified Numeric                    as NUM (readHex, showHexInt)
--import           Text.Printf                (printf)

import qualified Crypto.Hash.RIPEMD160      as RIPEMD160 (hash)
import qualified Crypto.Hash.SHA256         as SHA256
import qualified Crypto.Hash.SHA512         as SHA512
import qualified Crypto.MAC.HMAC            as HMAC
import           Data.ByteString.Base58

import           ECC                        (compressedFromPrivate, moduloNAdd,
                                             secp256k1, uncompressedFromPrivate)
import           HexUtils


-- | mainnet "80"
-- | testnet "ef"
kMAINNETTESTNET = packHex "80"

checksum = BS.take 4 . SHA256.hash . SHA256.hash . checksumBase
checksumUncompressed =
           BS.take 4 . SHA256.hash . SHA256.hash . checksumBaseUncompressed
checksumBase a = BS.concat [ kMAINNETTESTNET
                           , a
                           , packHex "01" ]
checksumBaseUncompressed a = BS.concat [ kMAINNETTESTNET
                                       , a ]
dec = fromJust . decodeBase58 bitcoinAlphabet

hmac512 = HMAC.hmac SHA512.hash 128
hashseed = hmac512 (BS.pack "Bitcoin seed")


masterKeyFromSeed prefix keyPart seed =
  encodeBase58 bitcoinAlphabet $
  BS.append
  (base privkey chaincode)
  (BS.take 4 $ SHA256.hash . SHA256.hash $ base privkey chaincode)
    where
    base privkey chaincode = BS.concat
      [ prefix
      , packHex "000000000000000000"
      , chaincode
      , keyPart ]
    privkey = BS.take 32 (hashseed seed)
    chaincode = BS.drop 32 (hashseed seed)

privatePartFromSeed seed = BS.take 32 . hashseed $ packHex seed
zeroPrivateFromSeed seed = BS.append (packHex "00") (privatePartFromSeed seed)
publicPartFromSeed seed = (compressedFromPrivate secp256k1 . readHexInt $ privatePartFromSeed seed )


-- fingerprint x = BS.take 4 . SHA256.hash . RIPEMD160.hash . compressedAddressFromSecretHex . BS.unpack $ privkey
--   where
--     privkey = BS.take 32 x






deserializeBip32PrivkeyChaincode :: String -> (BS.ByteString, BS.ByteString)
deserializeBip32PrivkeyChaincode x =
  ( BS.drop 46 . BS.take 78 . dec $ BS.pack x
  , BS.drop 13 . BS.take 45 . dec $ BS.pack x)

deserializeBip32PubkeyChaincode :: String -> (BS.ByteString, BS.ByteString)
deserializeBip32PubkeyChaincode x =
  ( BS.drop 45 . BS.take 78 . dec $ BS.pack x
  , BS.drop 13 . BS.take 45 . dec $ BS.pack x)


-- |
-- | PUBLIC INTERFACE
-- |
-- | reference:
-- | https://github.com/bitcoinbook/bitcoinbook/blob/develop/ch04.asciidoc
-- |


extPrivkeyFromRootPrivkey x index depth = serializeBip32PrivateKey (fst ll) (snd ll) index depth
  --unpackHex (fst ll)
  where
  ll = childCodeDerivationPrivate (fst l) (snd l) 0
  l = deserializeBip32PrivkeyChaincode x



------ TODO
childCodeDerivationPrivate :: BS.ByteString -> BS.ByteString -> Integer -> (BS.ByteString, BS.ByteString)
childCodeDerivationPrivate privkey chaincode index =
  (
    BS.take 32 baseI,
--    BSL.toStrict . BIN.encode $ newPrivkey ,
    BS.drop 32 baseI)
    where
    newPrivkey = moduloNAdd
                 (readHexInt privkey)
                 (readHexInt (BS.take 32 baseI))

    baseI = hmac512 secret message
    secret = leadZeroBytes 32 chaincode
    message
      | index >= 2147483648 =
          BS.concat [ packHex "00"
                    , leadZeroBytes 32 privkey
                    , leadZeroBytes 4 $ BSL.toStrict (BIN.encode index)]
      | otherwise = BS.append
          (compressedFromPrivate secp256k1 $ readHexInt privkey)
          (leadZeroBytes 4 $ BSL.toStrict (BIN.encode index))

childCodeDerivationPublic :: BS.ByteString -> BS.ByteString -> Integer -> (BS.ByteString, BS.ByteString)
childCodeDerivationPublic pubkey chaincode index =
  ((BS.take 32 baseI), BS.drop 32 baseI)
    where
    baseI = hmac512 secret message
    secret = leadZeroBytes 32 chaincode
    message
      | index >= 2147483648 = error "Invalid for hardened keys"
      | otherwise = BS.append pubkey
          (leadZeroBytes 4 $ BSL.toStrict (BIN.encode index))
-----




serializeBip32PrivateKey :: BS.ByteString -> BS.ByteString -> Int -> Integer -> String
serializeBip32PrivateKey privkey chaincode index depth =
--  unpackHex $ base privkey chaincode
  BS.unpack $ encodeBase58 bitcoinAlphabet $
  BS.append
  (base privkey chaincode)
  (BS.take 4 $ SHA256.hash . SHA256.hash $ base privkey chaincode)
    where
    base privkey chaincode = BS.concat
      [ packHex "0488ADE4"
      , packHex (showHexInt depth)
      , parentFingerprint
      , childNum
      , chaincode
      , packHex "00"
      , privkey ]
    parentFingerprint = packHex "00000000"
    childNum = packHex "00000000"





-- |  path = [0,0,44]  -- m/0/0/44
-- |
-- | seed: fffcf9f6f3f0edeae7e4e1dedbd8d5d2cfccc9c6c3c0bdbab7b4b1aeaba8a5a29f9c999693908d8a8784817e7b7875726f6c696663605d5a5754514e4b484542
-- | priv: xprv9s21ZrQH143K31xYSDQpPDxsXRTUcvj2iNHm5NUtrGiGG5e2DtALGdso3pGz6ssrdK4PFmM8NSpSBHNqPqm55Qn3LqFtT2emdEXVYsCzC2U
-- | pub:  xpub661MyMwAqRbcFW31YEwpkMuc5THy2PSt5bDMsktWQcFF8syAmRUapSCGu8ED9W6oDMSgv6Zz8idoc4a6mr8BDzTJY47LJhkJ8UB7WEGuduB
-- | m/0 priv: xprv9vHkqa6EV4sPZHYqZznhT2NPtPCjKuDKGY38FBWLvgaDx45zo9WQRUT3dKYnjwih2yJD9mkrocEZXo1ex8G81dwSM1fwqWpWkeS3v86pgKt
-- | m/0 pub:  xpub69H7F5d8KSRgmmdJg2KhpAK8SR3DjMwAdkxj3ZuxV27CprR9LgpeyGmXUbC6wb7ERfvrnKZjXoUmmDznezpbZb7ap6r1D3tgFxHmwMkQTPH
-- |



{-

  BS.unpack $ encodeBase58 bitcoinAlphabet $
  BS.append
  (base privkey chaincode)
  (BS.take 4 $ SHA256.hash . SHA256.hash $ base privkey chaincode)
    where
    base privkey chaincode = BS.concat
      [ packHex "0488ADE4"
      , packHex (showHexInt (length path))
      , parentFingerprint
      , leadZeroBytes 4 (packHex (showHexInt index))
      , chaincode
      , packHex "00"
      , privkey ]
    parentFingerprint
      | length path == 0 = packHex "00000000"
      | otherwise = -- packHex "00000000"
           fingerprint privkey --  $ bip32ExtendedPrivateKey seed1 path1 0
--         where path1 = []
--               seed1 = BS.unpack $ BS.append
--                       privkey
--                       chaincode
    fingerprint x = BS.take 4 . SHA256.hash . RIPEMD160.hash . compressedAddressFromSecret $ x

    privkey = BS.take 32 (hash seed)
    chaincode = BS.drop 32 (hash seed)
    hash seed = leadZeroBytes 64 $ hmac512 (BS.pack "Bitcoin seed") (packHex seed)

    derivHash = hmac512 secret message
    secret = leadZeroBytes 32 $ packHex chaincode
    message
      | index >= 2147483648 =
          BS.concat [ packHex "00"
                    , leadZeroBytes 32 $ packHex privkey
                    , leadZeroBytes 4 $ BSL.toStrict (BIN.encode index)]
      | otherwise = BS.append
          (compressedFromPrivate secp256k1 $ readHex privkey)
          (leadZeroBytes 4 $ BSL.toStrict (BIN.encode index))

-}






-- | BIP-32
-- |


bech32MasterPrivkeyFromSeed :: String -> String
bech32MasterPrivkeyFromSeed seed = BS.unpack $ masterKeyFromSeed (packHex "04b2430c") (zeroPrivateFromSeed seed) $ (packHex seed)

bech32MasterPubkeyFromSeed  :: String -> String
bech32MasterPubkeyFromSeed  seed = BS.unpack $ masterKeyFromSeed (packHex "04b24746") (publicPartFromSeed seed)  $ (packHex seed)


segwitMasterPrivkeyFromSeed :: String -> String
segwitMasterPrivkeyFromSeed seed = BS.unpack $ masterKeyFromSeed (packHex "049d7878") (zeroPrivateFromSeed seed) $ (packHex seed)

segwitMasterPubkeyFromSeed  :: String -> String
segwitMasterPubkeyFromSeed  seed = BS.unpack $ masterKeyFromSeed (packHex "049d7cb2") (publicPartFromSeed seed)  $ (packHex seed)


normalMasterPrivkeyFromSeed :: String -> String
normalMasterPrivkeyFromSeed seed = BS.unpack $ masterKeyFromSeed (packHex "0488ADE4") (zeroPrivateFromSeed seed) $ (packHex seed)

normalMasterPubkeyFromSeed  :: String -> String
normalMasterPubkeyFromSeed  seed = BS.unpack $ masterKeyFromSeed (packHex "0488B21E") (publicPartFromSeed seed)  $ (packHex seed)



-- | not recommended for use
segwitAddressFromCompressedAddress :: String -> String
segwitAddressFromCompressedAddress x = BS.unpack . encodeBase58 bitcoinAlphabet $ BS.append
       (base x)
       (BS.take 4 . SHA256.hash . SHA256.hash . base $ x)
       where base = BS.append (packHex "05") . RIPEMD160.hash . SHA256.hash . BS.append (packHex "0014") . BS.take 20 . BS.drop 1 . fromJust . decodeBase58 bitcoinAlphabet . BS.pack



-- | in:  KwHCs714w3m74i8Pxn6vDzi32r8kPczLqkwELeuwrFt8aCMSMy7V
-- | out: 3HN9t38cctHZj6pPcSjT4fUpGGneGEEG37
segwitAddressFromWIF :: String -> String
segwitAddressFromWIF = unpackHex . segwitAddressFromSecret . secretFromWIF . BS.pack

compressedAddressFromWIF :: String -> String
compressedAddressFromWIF = unpackHex . compressedAddressFromSecret . secretFromWIF . BS.pack

uncompressedAddressFromWIF :: String -> String
uncompressedAddressFromWIF = unpackHex . uncompressedAddressFromSecret . secretFromWIF . BS.pack



segwitAddressFromSecret :: BS.ByteString ->  BS.ByteString
segwitAddressFromSecret x =  encodeBase58 bitcoinAlphabet $ BS.append (base x) (BS.take 4 . SHA256.hash . SHA256.hash . base $ x)
  where base s = BS.append (packHex "05") . RIPEMD160.hash . SHA256.hash . BS.append (packHex "0014") $ wprog s
        wprog = RIPEMD160.hash . SHA256.hash . compressedFromPrivate secp256k1 . readHexInt

compressedAddressFromSecret :: BS.ByteString -> BS.ByteString
compressedAddressFromSecret x =  encodeBase58 bitcoinAlphabet $ BS.append (base x) (BS.take 4 . SHA256.hash . SHA256.hash . base $ x)
  where base s = BS.append (packHex "00") $ wprog s
        wprog = RIPEMD160.hash . SHA256.hash . compressedFromPrivate secp256k1 . readHexInt

uncompressedAddressFromSecret :: BS.ByteString -> BS.ByteString
uncompressedAddressFromSecret x =  encodeBase58 bitcoinAlphabet $ BS.append (base x) (BS.take 4 . SHA256.hash . SHA256.hash . base $ x)
  where base s = BS.append (packHex "00") $ RIPEMD160.hash . SHA256.hash . uncompressedFromPrivate secp256k1 . readHexInt $ s


wifFromSecret :: BS.ByteString -> BS.ByteString
wifFromSecret = encodeBase58 bitcoinAlphabet . appendChecksum
  where
    appendChecksum a = BS.append (checksumBase a) (checksum a)

secretFromWIF :: BS.ByteString -> BS.ByteString
secretFromWIF a
  | wifValid a = BS.drop 1 $ BS.take 33 (dec a)
  | otherwise = BS.empty

wifValid :: BS.ByteString -> Bool
wifValid a
  | isNothing (decodeBase58 bitcoinAlphabet a) = False
  | otherwise = kMAINNETTESTNET == BS.take 1 (dec a) &&
      (BS.take 4 . SHA256.hash . SHA256.hash $ BS.take 34 (dec a))
      == BS.drop 34 (dec a)

keyValid :: BS.ByteString -> Bool
keyValid a
  | isNothing (decodeBase58 bitcoinAlphabet a) = False
  | otherwise =
      (BS.take 4 . SHA256.hash . SHA256.hash $ BS.take (len a) (dec a))
      ==
      BS.drop (len a) (dec a)
      where
        len a
          | BS.length (dec a) < 4 = 0
          | otherwise = BS.length (dec a) - 4

