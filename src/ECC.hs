-- for secp-256k1, see http://www.secg.org/sec2-v2.pdf

module ECC ( EllipticCurve
           , PrivateKey
           , PublicGeneralKey
           , PublicSpecificKey
           , PublicKey(PublicKey)
           , generateRandomPoint
           , generateECCGeneralKey
           , generateECCSpecificKey
           , encryptECC
           , decryptECC
           , secp256r1
           , secp256k1
           , compressedFromPrivate
           , uncompressedFromPrivate
           , moduloNAdd
           ) where

import qualified Data.ByteString.Char8 as BS (ByteString, append, concat)
import           Data.Maybe
import           System.IO
import           System.Random

import           HexUtils              (packHex, readHex, showHexInt)




moduloNAdd :: Integer -> Integer -> Integer
moduloNAdd a b = ((a % m) + (b % m)) % m
  where
    m = getN ECC.secp256k1
    getN (ECC.EllipticCurve m a b g n h) = n



secp256r1 = EllipticCurve 115792089210356248762697446949407573530086143415290314195533631308867097853951 (-3) 0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b (Point 0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296 0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5) 115792089210356248762697446949407573529996955224135760342422259061068512044369 1

secp256k1 = EllipticCurve 115792089237316195423570985008687907853269984665640564039457584007908834671663 0 0x07 (Point 0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798 0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8) 115792089237316195423570985008687907852837564279074904382605163141518161494337 1




-- For an elliptic curve to form a group, we require that
checkValidEllipticCurve :: (Integer, Integer, Integer) -> Bool
checkValidEllipticCurve (a,b,p) = (4 * (a^^^3 |% p) + 27 * (b^^^2 |% p)) % p == 0

-- Discussion of [group theory](https://www.certicom.com/10-introduction)

data Point = Point Integer Integer | O deriving (Eq, Show, Read)
-- p    - base field prime
-- a, b - define the elliptic curve in E/F_p (the elliptic curve is also referred to as E_p(a,b))
-- g    - base point (hopefully a generator) on the elliptic curve
-- n    - |g| (order of g)
-- h    - |E/F_p|/|g| (ideally, if g is a generator, this is 1)
-- In order: p a b g n h
data EllipticCurve = EllipticCurve Integer Integer Integer Point Integer Integer deriving(Show, Read)

-- Elliptic curve operations over prime fields
(<++>) :: Point -> Point -> EllipticCurve -> Point
(<++>) O p _ = p
(<++>) p O _ = p
(<++>) p@(Point x_p y_p) q@(Point x_q y_q) (EllipticCurve m a b g n h)
  | p /= q && x_p == x_q = O
  | otherwise            = do
                             let s = if p == q
                                       then (3*x_p^2 + a) *** (invert (2*y_p) |% m)     |% m
                                       else (y_p - y_q)   *** (invert (x_p - x_q) |% m) |% m
                                 x_r = s^2 - x_p - x_q
                                 y_r = -y_p + s*(x_p - x_r)
                             Point (x_r % m) (y_r % m)

negative :: Point -> Point
negative (Point x_p y_p) = Point x_p (-y_p)

(<**>) :: Integer -> Point -> EllipticCurve -> Point
(<**>) a p ec = helper a O p
  where
    helper :: Integer -> Point -> Point -> Point
    helper p x a | p==0  = x
                 | p>0   = helper (p `quot` 2)
                                  (if (even p) then x else (a <++> x $ ec))
                                  (a <++> a $ ec)

-- PRIMITIVES:

-- NOTES: permits us to define infix functions with signature 'a -> a -> a' and call them
-- using a more suggestive notation 'x ^^^ y |% m' (for function (^^^) :: a -> a -> a)
(|%) = ($)
infixr 1 |%

-- Yields the remainder from the division of the first argument by the second.
-- NOTES: The result has the same sign as the second argument.
(%) :: (Integral a) => a -> a -> a
x % p = if y >= 0 then y else y + p
  where y = x `mod` p
infix 6 %

-- Calculates the bezout coefficients of a given pair of numbers, and their gcd.
-- (Given a and b, it returns [x,y,g] where a*x + b*y = g and g is the gcd.)
bezout :: (Integral a) => a -> a -> [a]
bezout x y = bezout [1,0,x] [0,1,y]
  where bezout u v
          | v!!2==0   = u
          | otherwise = let q = (u!!2) `div` (v!!2)
                        in bezout v [u!!k - q * v!!k | k <- [0..2]]

-- Finds the modular multiplicative inverse
-- NOTES: The number and the mod must be relatively prime for an answer to exist. If this is not
-- the case, no inverse exists, and "invert" returns 0.
-- (Given a and m, it returns x where a*m = 1 (mod m).)
invert :: (Integral a) => a -> a -> a
invert x y | b==1      = a % y
           | otherwise = 0
  where [a,_,b] = bezout x y


-- Finds the modular product
-- NOTES: uses the fact that a*b (mod m) is equal to c*d (mod m) provided a = c (mod m) and
-- b = d (mod m). Exploits the fact '%' is much faster than '*'.
-- (Given a, b, and m, it returns x where x = a*b (mod m))
(***) :: (Integral a) => a -> a -> a -> a
(***) a b m = ((a % m) * (b % m)) % m

-- Finds the modular addition
-- NOTES: uses the fact that a+b (mod m) is equal to c+d (mod m) provided a = c (mod m) and
-- b = d (mod m). Exploits the fact '%' is much faster than '+'.
-- (Given a, b, and m, it returns x where x = a+b (mod m))
(+++) :: (Integral a) => a -> a -> a -> a
(+++) a b m = ((a % m) + (b % m)) % m

-- Finds the modular exponent
-- NOTES: Usual exponentiation by squaring, but for negative exponent take a^(-k) = (1/a)^k where
-- (1/a) is found through inverting in the given modulo.
-- (Given a, k, and m, it returns x where x = a^k (mod m))
(^^^) :: (Integral a) => a -> a -> a -> a
(^^^) a k m = helper k 1 a
  where
    helper k x a
      | k==0  = x % m
      | k>0   = helper (k `quot` 2) (if (even k) then x else (a*x % m)) ((a*a) % m)
      | k<0   = (invert a m) ^^^ (negate k) |%  m


------------------------------------------------------

-- ECC Encryption
-- ==============

-- Types representing public and private keys
data PrivateKey        = PrivateKey Integer Point            -- (alpha a)
data PublicGeneralKey  = PublicGeneralKey Point Point Point  -- (a_1 a_2 c)
data PublicSpecificKey = PublicSpecificKey Point             -- (a_b)
data PublicKey         = PublicKey PublicGeneralKey PublicSpecificKey

generateRandomPoint :: EllipticCurve -> IO Point
generateRandomPoint ec@(EllipticCurve p a b g n h)
  = randomRIO (1, n-1) >>= return . (\d -> d <**> g $ ec)



-- Given an elliptic curve and a random point c, generate a private key (a alpha) and a general
-- public key (a_1 a_2).
generateECCGeneralKey :: EllipticCurve -> Point -> IO (PrivateKey, PublicGeneralKey)
generateECCGeneralKey ec@(EllipticCurve _ _ _ _ n _) c
  = do
      alpha <- randomRIO (1, n-1)     -- 'alpha' is a random number less than the order of E_p(a,b)
      a     <- generateRandomPoint ec -- 'a' is therefore a random element of the curve
      let a_1 = alpha <**> (c <++> a $ ec) $ ec
          a_2 = alpha <**> a              $ ec
      return (PrivateKey alpha a, PublicGeneralKey a_1 a_2 c)

-- Given an elliptic curve, one's private key, and someone else's public general key, generate a
-- public specific key for that party.
generateECCSpecificKey :: EllipticCurve -> PrivateKey -> PublicGeneralKey -> PublicSpecificKey
generateECCSpecificKey ec (PrivateKey alpha _) (PublicGeneralKey _ b_2 _)
  = PublicSpecificKey (alpha <**> b_2 $ ec)

-- Encrypt a list of points using one's private key and another party's public key
encryptECC :: [Point] -> EllipticCurve -> PrivateKey -> PublicKey -> IO [(Point,Point)]
encryptECC points ec@(EllipticCurve _ _ _ _ n _) private public
  = mapM encryptPoint points
    where
      encryptPoint :: Point -> IO (Point, Point)
      encryptPoint point
        = do
            gamma <-  randomRIO (1, n-1)
            let (PublicKey (PublicGeneralKey a_1 a_2 c) (PublicSpecificKey a_b)) = public
                (PrivateKey beta b)                                              = private
                e_1 = gamma <**> c $ ec
                e_2 = (point <++> ((beta + gamma) <**> a_1 $ ec) $ ec)
                        <++> ((negative (gamma <**> a_2 $ ec)) <++> a_b $ ec) $ ec
            return (e_1, e_2)

-- Decrypt a list of pairs of points using one's private key and another party's public key
decryptECC :: [(Point,Point)] -> EllipticCurve -> PrivateKey -> PublicKey -> [Point]
decryptECC points ec private public
  = let (PublicKey (PublicGeneralKey b_1 b_2 _) (PublicSpecificKey b_a)) = public
        (PrivateKey alpha a)                                             = private
    in map (\(e_1, e_2) ->
                   (e_2 <++> (negative ((alpha <**> (e_1 <++> b_1 $ ec) $ ec) <++> b_a $ ec)) $ ec)) points

-- Brute Force Decryption
-- ======================

-- Find n such that n <**> p = q
discreteLog :: Point -> Point -> EllipticCurve -> Integer
discreteLog p q ec = let (_,n) = until (\(x,n) -> q==x) (\(x,n) -> (x <++> p $ ec, n+1)) (p,1) in n



-- |
-- | PUBLIC INTERFACE
-- |

-- Find point (public key) for a given integer (private key)
compressedFromPrivate :: EllipticCurve -> Integer -> BS.ByteString
compressedFromPrivate ec@(EllipticCurve _ _ _ g _ _) x =
  compressedPoint $ (<**>) x g ec
  where
    compressedPoint p = case p of
      O -> packHex "0200000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
      (Point x y) -> if even y then BS.append (packHex "02") (packHex (showHexInt x))
                     else BS.append (packHex "03") (packHex (showHexInt x))


uncompressedFromPrivate :: EllipticCurve -> Integer -> BS.ByteString
uncompressedFromPrivate ec@(EllipticCurve _ _ _ g _ _) x =
  uncompressedPoint $ (<**>) x g ec
  where
    uncompressedPoint p = case p of
      O -> packHex "0400000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"
      (Point x y) -> BS.concat [packHex "04" ,  packHex (showHexInt x) ,  packHex (showHexInt y)]



-- A public key is a point (X, Y). Its serialization is 04+X+Y as uncompressed, and (02+X as compressed if Y is even), and (03+X as compressed if Y is odd). X and Y are here the corresponding 64-character hexadecimal string

