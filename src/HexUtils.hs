module HexUtils (  readHex
                 , readHexInt
                 , showHexInt
                 , packHex
                 , unpackHex
                 , leadZeroBytes
                 ) where

import qualified Data.ByteString.Char8      as BS
import qualified Data.ByteString.Lazy.Char8 as BSL
import           Data.List



unchar a
  | x > 15    = x - 6
  | otherwise = x
  where
    x = fromJust' (elemIndex a "0123456789abcdefABCDEF")
    fromJust' :: Num a => Maybe a -> a
    fromJust' Nothing  = error "ERROR: Wrong HEX symbols"
    fromJust' (Just x) = x


-- |
-- | PUBLIC INTERFACE
-- |

readHex :: String -> Integer
readHex hxStr = go (reverse hxStr)
  where
  go []     = 0
  go (x:xs) = hexChar x + 16 * go xs
  hexChar ch
    | ch == '0' = 0
    | ch == '1' = 1
    | ch == '2' = 2
    | ch == '3' = 3
    | ch == '4' = 4
    | ch == '5' = 5
    | ch == '6' = 6
    | ch == '7' = 7
    | ch == '8' = 8
    | ch == '9' = 9
    | ch == 'a' || ch == 'A' = 10
    | ch == 'b' || ch == 'B' = 11
    | ch == 'c' || ch == 'C' = 12
    | ch == 'd' || ch == 'D' = 13
    | ch == 'e' || ch == 'E' = 14
    | ch == 'f' || ch == 'F' = 15
    | otherwise = error "STRING IS NOT HEX"

readHexInt :: BS.ByteString -> Integer
readHexInt = readHex . unpackHex





showHexInt :: (Integral a) => a -> String
showHexInt x = toKBaseNum x 16
  where
  toKBaseNum x base
    | x < base  = [go x]
    | otherwise = toKBaseNum (x `div` base) base ++ [go(x `mod` base)]
  go n
    | n == 0 = '0'
    | n == 1 = '1'
    | n == 2 = '2'
    | n == 3 = '3'
    | n == 4 = '4'
    | n == 5 = '5'
    | n == 6 = '6'
    | n == 7 = '7'
    | n == 8 = '8'
    | n == 9 = '9'
    | n == 10 = 'a'
    | n == 11 = 'b'
    | n == 12 = 'c'
    | n == 13 = 'd'
    | n == 14 = 'e'
    | n == 15 = 'f'
    | otherwise = error "NON-INTEGER VALUE"

packHex :: String -> BS.ByteString
packHex [] = BS.empty
packHex [b] = BS.singleton (toEnum (unchar b) :: Char)
packHex (a:b:rest) = BS.append
  (BS.singleton (toEnum ((unchar a * 16) + unchar b) :: Char))
  (packHex rest)


unpackHex :: BS.ByteString -> String
unpackHex = concatMap w . BS.unpack
  where
    w ch =
      let s = "0123456789abcdef"
          x = fromEnum ch
      in [s !! div x 16,s !! mod x 16]

leadZeroBytes :: Int -> BS.ByteString -> BS.ByteString
leadZeroBytes count bytes
  | BS.length bytes >= count = bytes
  | otherwise = leadZeroBytes count (BS.append (packHex "00") bytes)
